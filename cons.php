<?php
$username = "root";
$password = "";
$servername = "localhost";
define('base_url','http://localhost/generictest/');
try {
	$conn = new PDO("mysql:host=$servername;dbname=test", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
    echo $e->getMessage();
}
	
function listDirArrForm($dir="upload/",$dir_parent=0,$parent=0) {
  global $conn;
   $result = array();
   if(is_dir($dir)){
	   //$conn->beginTransaction();
	   $flag=TRUE;
	   $sql = "INSERT INTO directory (name,parent,file_type,dir_id) VALUES (:name,:parent,:file_type,:dir_id)";
	   $q = $conn->prepare($sql);
	   $cdir = scandir($dir);
	   foreach ($cdir as $key => $value)
	   {
		  if (!in_array($value,array(".","..")))
		  {
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
			 {
				 $file_type = 'D';
				$q->execute(array(':name'=>$value,':parent'=>$parent,':file_type'=>$file_type,':dir_id'=>$dir_parent));
				$parent=$conn->lastInsertId();
				$result[$value] = listDirArrForm($dir . DIRECTORY_SEPARATOR . $value,$dir_parent,$parent);
				
			 }
			 else
			 {
				$result[] = $value;
				$file_type = 'F';
				$q->execute(array(':name'=>$value,':parent'=>$parent,':file_type'=>$file_type,':dir_id'=>$dir_parent));
				$count=$q->rowCount();
				if($count==0){
					$flag=FALSE;
					break;
				}
			 }
			 
		  }
	   }
	   
   }
   return $result;
} 

function directory_tree($dir_id,$parent=0){
	global $conn;
	
	$sql = "select * from directory where parent =:parent AND dir_id=:dir_id";
	$result = $conn->prepare($sql);
	$result->execute(array(':dir_id'=>$dir_id,':parent'=>$parent));
	$results = $result->fetchAll(PDO::FETCH_ASSOC);
	foreach($results as $row){
	$i = 0;
	if ($i == 0) echo '<ul>';
	 echo '<li>' . $row['name'];
	 directory_tree($dir_id,$row['id']);
	 echo '</li>';
	$i++;
	 if ($i > 0) echo '</ul>';
	}
}

function listDirArrView($dir="upload/") {
  
   $result = array();
   if(is_dir($dir)){
	  
	   $cdir = scandir($dir);
	   foreach ($cdir as $key => $value)
	   {
		  if (!in_array($value,array(".","..")))
		  {
			 if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
			 {
				$result[$value] = listDirArrView($dir . DIRECTORY_SEPARATOR . $value);
			 }
			 else
			 {
				$result[] = $value;
			 }
			 
		  }
	   }
   }
   return $result;
} 
	
	?>