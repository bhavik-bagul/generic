<?php
/**
 * Directory/Folder Tree
 * @author Bhavik
 */
$arr = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	require 'cons.php';
	if($_POST['dirpath']!=''){
		$dir = $_POST['dirpath'];
		global $conn;
		$sql = "INSERT INTO dir_mapping (name) VALUES (:name)";
	    $q = $conn->prepare($sql);
		$q->execute(array(':name'=>'Dir-'.time()));
		$parent=$conn->lastInsertId();
		$arr = listDirArrForm($dir,$parent);
		header("location:listing.php");
	}
}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Directory Tree</title>      
    <link href="bootstrap.min.css" rel="stylesheet" type="text/css">
    </head>    
    <body>
        <div class="row">
            <div class="col-lg-8">
            <h1>Folder/Directory Upload using HTML and PHP</h1>
            <form method="post" enctype="multipart/form-data" class="form-horizontal">
                <label class="col-sm-3">Enter Directory path here</label>
                <div class="col-sm-9"><input type="text" name="dirpath" class="form-control"></div>
                <div class="col-sm-3"></div><div class="col-lg-9"><input type="submit" value="Upload" class="btn" /></div>
            </form>           
        </div>
    </body>
</html>